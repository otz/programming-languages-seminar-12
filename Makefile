.PHONY = all diff clean

CC = gcc
CFLAGS = -std=c17 -Wall

good: 
	$(CC) $(CFLAGS) -o good optional.c vector.c good.c

bad: 
	$(CC) $(CFLAGS) -o bad bad.c

all: good bad

diff:
	diff -s --label GOOD-OUTPUT <(./good) --label BAD-OUTPUT <(./bad) 

clean:
	rm -f good bad