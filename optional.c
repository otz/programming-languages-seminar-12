#include <stdbool.h>
#include <inttypes.h>
#include "optional.h"

struct optional optional_some(int64_t value) {
    return (struct optional) { value, true };
}

struct optional optional_none = (struct optional) {0, false };