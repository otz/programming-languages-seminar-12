#include <inttypes.h>
#include <stdio.h>
#include <stdarg.h>

#include "vector.h"

void print_item(int64_t item, va_list args) {
    fprintf(va_arg(args, FILE*), "%" PRId64 " ", item);
}

void print_vector(FILE* f, struct vector* vec) {
    vector_foreach(vec, print_item, f);
}

int main() {
    struct vector* vec = vector_with_capacity_hint((size_t)5);
    for (int64_t i = 0; i < 100; i++) {
        vector_push(vec, i * i);
    }
    print_vector(stdout, vec);
    vector_destroy(vec);
    return 0;
}
