#include "vector.h"

#include <malloc.h>
#include <string.h>

struct vector {
    int64_t *data;
    size_t length;
    size_t capacity;
};

// For static functions with `raw` in the name, all parameters
// are assumed to be valid (e.g. index < vec->length)

static bool vector_index_is_inbound(const struct vector *vec, size_t index) {
    return (index < vec->length) ? (index < vec->length) : false;
}

static int64_t vector_raw_get(const struct vector *vec, size_t index) {
    return vec->data[index];
}

static void vector_raw_set(struct vector *vec, int64_t value, size_t index) {
    vec->data[index] = value;
}

static void vector_raw_shift(struct vector *vec, size_t index, ssize_t shift) {
    int64_t *const base = vec->data + index;
    const size_t chunk_size = sizeof(int64_t) * (vec->length - index);
    memmove((void *)(base + shift), (void *)(base), chunk_size);
    vec->length += shift;
}

static void vector_raw_remove(struct vector *vec, size_t index) {
    const size_t next_index = index + 1;
    if (next_index == vec->length) {
        vec->length--;
    } else {
        vector_raw_shift(vec, next_index, (ssize_t)-1);
    }
}

static bool vector_reallocate(struct vector *vec, size_t new_capacity) {
    int64_t *const data = reallocarray(vec->data, sizeof(int64_t), new_capacity);
    if (data != NULL) {
        vec->data = data;
        vec->capacity = new_capacity;
        return true;
    } else {
        return false;
    }
}

static bool vector_reserve(struct vector *vec, size_t space) {
    const size_t desired_size = vec->length + space;
    if (desired_size <= vec->capacity) {
        return true;
    } else {
        size_t new_capacity = (vec->capacity == 0) ? (1) : (vec->capacity << 1);
        while (desired_size > new_capacity) {
            new_capacity <<= 1;
        }
        return vector_reallocate(vec, new_capacity);
    }
}

static bool vector_shrink(struct vector *vec) {
    if (vec->capacity != 0 && vec->length <= vec->capacity >> 1) {
        size_t shrank_capacity = vec->capacity;
        do {
            shrank_capacity >>= 1;
        } while (vec->length <= shrank_capacity >> 1);
        return vector_reallocate(vec, shrank_capacity);
    } else {
        return false;
    }
}

struct vector *vector_new() {
    struct vector *const vec = malloc(sizeof(struct vector));
    vec->data = NULL;
    vec->length = 0;
    vec->capacity = 0;
    return vec;
}

struct vector *vector_with_capacity_hint(size_t capacity) {
    struct vector *const vec = vector_new();
    vector_reserve(vec, capacity);
    return vec;
}

void vector_hint_shrink(struct vector *vec) {
    vector_shrink(vec);
}

void vector_destroy(struct vector *vec) {
    if (vec->data != NULL) {
        free(vec->data);
    }
    free(vec);
}

struct optional vector_get(const struct vector *vec, size_t index) {
    return (vector_index_is_inbound(vec, index))
               ? optional_some(vector_raw_get(vec, index))
               : optional_none;
}

bool vector_set(struct vector *vec, int64_t value, size_t index) {
    if (vector_index_is_inbound(vec, index)) {
        vector_raw_set(vec, value, index);
        return true;
    } else {
        return false;
    }
}

bool vector_insert(struct vector *vec, int64_t value, size_t index) {
    if (vector_index_is_inbound(vec, index) && vector_reserve(vec, (size_t)1)) {
        vector_raw_shift(vec, index, (ssize_t)1);
        vector_raw_set(vec, value, index);
        return true;
    } else {
        return false;
    }
}

bool vector_remove(struct vector *vec, size_t index) {
    if (vector_index_is_inbound(vec, index)) {
        vector_raw_remove(vec, index);
        return true;
    } else {
        return false;
    }
}

bool vector_push(struct vector *vec, int64_t value) {
    if (vector_reserve(vec, (size_t)1)) {
        vector_raw_set(vec, value, vec->length);
        vec->length++;
        return true;
    } else {
        return false;
    }
}

struct optional vector_pop(struct vector *vec, size_t index) {
    const struct optional item = vector_get(vec, index);
    if (item.present) {
        vector_raw_remove(vec, index);
    }
    return item;
}

void vector_foreach(struct vector *vec, void func(int64_t, va_list), ...) {
    if (vec->data != NULL) {
        va_list csargs;
        va_start(csargs, func);
        for (size_t i = 0; i < (vec->length); i++) {
            va_list cargs;
            va_copy(cargs, csargs); 
            func(vec->data[i], cargs);
            va_end(cargs);
        }
    }
}