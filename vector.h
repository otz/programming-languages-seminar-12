#ifndef ITMO_PROGRAMMING_LANGUAGES_VECTOR_H
#define ITMO_PROGRAMMING_LANGUAGES_VECTOR_H

#include <sys/types.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include "optional.h"

struct vector;

struct vector *vector_new();

struct vector *vector_with_capacity_hint(size_t capacity);

void vector_hint_shrink(struct vector *vec);

void vector_destroy(struct vector* vec);

struct optional vector_get(const struct vector *vec, size_t index);

bool vector_set(struct vector *vec, int64_t value, size_t index);

bool vector_insert(struct vector *vec, int64_t value, size_t index);

bool vector_remove(struct vector *vec, size_t index);

bool vector_push(struct vector *vec, int64_t value);

struct optional vector_pop(struct vector *vec, size_t index);

void vector_foreach (struct vector *vec, void func(int64_t, va_list), ...);

#endif
