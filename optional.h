#ifndef ITMO_PROGRAMMING_LANGUAGES_OPTIONAL_H
#define ITMO_PROGRAMMING_LANGUAGES_OPTIONAL_H

#include <stdbool.h>
#include <inttypes.h>

struct optional {
    int64_t value;
    bool present;
};

struct optional optional_some(int64_t value);

extern struct optional optional_none;

#endif
